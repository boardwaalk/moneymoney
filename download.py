#!/usr/bin/env python3
import argparse
import calendar
import datetime
import io
import json
import os
import os.path
import pandas
import re
import requests

def login(session, username, password):
   form = {
      'SSN': username,
      'PIN': password
   }

   resp = session.post('https://login.fidelity.com/ftgw/Fas/Fidelity/RtlCust/Login/Response', form)
   resp.raise_for_status()
   resp.text.index('<title>Redirect to Default Page</title>')

def quarter_begin(y, q):
    assert q >= 1 and q <= 4
    m = (q - 1) * 3 + 1
    d = 1
    return datetime.date(y, m, d)

def quarter_end(y, q):
    assert q >= 1 and q <= 4
    m = (q - 1) * 3 + 3
    _, d = calendar.monthrange(y, m)
    return datetime.date(y, m, d)

def quarter_add(y, q, n):
    q += n
    while q < 1:
        y -= 1
        q += 4
    while q > 4:
        y += 1
        q -= 4
    return y, q

def fetch_account_history_quarter(session, account, year, quarter):
    from_date = quarter_begin(year, quarter)
    to_date = min(quarter_end(year, quarter), datetime.date.today())

    from_month = from_date.strftime('%b')
    to_month = to_date.strftime('%b')

    query = {
        'ACCOUNT': account,
        'ACCT_HIST_SORT': 'DATE',
        'ACCT_HIST_DAYS': 'Q{0} {1} ({2}-{3})'.format(quarter, year, from_month, to_month),
        'FROM_DATE': from_date.strftime('%m/%d/%Y'),
        'TO_DATE': to_date.strftime('%m/%d/%Y'),
        'SORT_TYPE': 'D',
        'VIEW_TYPE': 'NON_CORE',
        'PAGE_TYPE': 'A',
        'CSV': 'Y',
        'Ref_at': 'Historydownload'
    }

    text = session.get('https://oltx.fidelity.com/ftgw/fbc/ofaccounts/brokerageAccountHistory', params=query).text

    # Remove extra whitespace
    text = re.sub(r'[ \t]+', ' ', text)

    # Remove disclaimer junk
    text = re.sub(r'^".+$', '', text, flags=re.M)

    # Remove empty lines
    text = os.linesep.join([s for s in text.splitlines() if s])

    # Parse into data frame
    df = pandas.io.parsers.read_csv(io.StringIO(text), skipinitialspace=True)

    # Reformat dates into something sortable
    def reformat_date(dt):
        if isinstance(dt, str) and len(dt) != 0:
            dt = datetime.datetime.strptime(dt, '%m/%d/%Y').strftime('%Y/%m/%d')
        return dt

    df['Run Date'] = df['Run Date'].apply(reformat_date)
    df['Settlement Date'] = df['Settlement Date'].apply(reformat_date)

    return df

def fetch_account_history(session, account):
    today = datetime.date.today()
    year = today.year
    quarter = (today.month - 1) // 3 + 1
    df = None

    while True:
        print('Fetching {0}/{1}'.format(year, quarter))
        quarter_df = fetch_account_history_quarter(session, account, year, quarter)
        if quarter_df.empty:
            break

        if df is None:
            df = quarter_df
        else:
            df = pandas.concat([df, quarter_df])

        year, quarter = quarter_add(year, quarter, -1)

    df = df.drop_duplicates()
    df = df.sort('Run Date')

    return df

def fetch_symbol_history(session, symbol, from_date, to_date):
    query = {
        's': symbol,
        'a': from_date.month,
        'b': from_date.day,
        'c': from_date.year,
        'd': to_date.month,
        'e': to_date.day,
        'f': to_date.year,
        'g': 'd',
        'ignore': '.csv'
    }

    resp = session.get('http://ichart.finance.yahoo.com/table.csv?', params=query)

    if resp.status_code == 404:
        return None

    resp.raise_for_status()
    return pandas.io.parsers.read_csv(io.StringIO(resp.text))

def main():
    with open(os.path.expanduser('~/.moneymoney.conf'), 'r') as f:
        conf = json.load(f)

    session = requests.Session()

    parser = argparse.ArgumentParser()
    parser.add_argument('--load', action='store_true')
    parser.add_argument('--fetch', action='store_true')
    parser.add_argument('--save', action='store_true')
    parser.add_argument('--totals', action='store_true')
    args = parser.parse_args()

    if args.load:
        history = pandas.io.parsers.read_csv('AccountHistory.csv')
        #symbols = history.groupby('Symbol')['Run Date'].min().to_dict()
        #for symbol, from_date_str in symbols.items():
        #    from_date = datetime.datetime.strptime(from_date_str, '%Y/%m/%d')
        #    to_date = datetime.date.today()
        #    print( fetch_symbol_history(session, symbol, from_date, to_date) )

    if args.fetch:
        login(session, conf['username'], conf['password'])
        history = fetch_account_history(session, conf['account'])

    if args.save:
        history.to_csv('AccountHistory.csv', index=False)

    if args.totals:
        buys = history[history['Action'].map(lambda a: a.startswith('YOU BOUGHT'))]
        buys = buys[['Symbol', 'Amount ($)']]
        print(buys.groupby('Symbol').sum())

if __name__ == "__main__":
    main()

